package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.DoubleRounder;
import utils.XmlHelper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExchangeRatesPage extends Page {

    @FindBy(xpath = ".//table[@class=\"data\"]/tbody/tr[position()>1]")
    private List<WebElement> table;

    public ExchangeRatesPage(WebDriver driver) {
        super(driver);
    }

    public void saveRateToXml(String fileName) {
        Map<String, Double> rateMap = new HashMap<>();
        table.forEach(row -> {
                    String key = row.findElement(By.xpath("td[2]")).getText();
                    Double value = Double.parseDouble(row.findElement(By.xpath("td[5]")).getText()
                            .replace(",","."));
                    rateMap.put(key, DoubleRounder.round(value, 2));
                }
        );
        XmlHelper.writeMapXmlFile(rateMap, fileName);
    }
}