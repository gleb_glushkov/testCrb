package steps;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;
import pages.ExchangeRatesPage;
import utils.XmlHelper;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;

import static utils.Constants.BASE_URL;

public class Steps {

    private WebDriver driver;

    private ExchangeRatesPage exchangeRatesPage;


    public Steps(WebDriver driver) {
        this.driver = driver;
        exchangeRatesPage = PageFactory.initElements(driver, ExchangeRatesPage.class);
    }

    public void goTo(String url){
        driver.navigate().to(url);
    }

    public void saveCurrentDayRate() {
        driver.navigate().to(BASE_URL +  "/currency_base/daily/");
        exchangeRatesPage.saveRateToXml("cdexchangeRate.xml");
    }

    public void savePastDayRate() {
        driver.navigate()
                .to(BASE_URL + "/currency_base/daily/?UniDbQuery.Posted=True&UniDbQuery.To=" + getPastDay());
        exchangeRatesPage.saveRateToXml("pdexchangeRate.xml");
    }

    public void compareRate() {
        Map<String, Double> currentDayMap = XmlHelper.readMapXmlFile("cdexchangeRate.xml");
        Map<String, Double> pastDayMap = XmlHelper.readMapXmlFile("pdexchangeRate.xml");

        SoftAssert asert = new SoftAssert();
        currentDayMap.forEach((countryCode, rate) ->
                asert.assertEquals(countryCode, pastDayMap.get(countryCode), countryCode + ": "));
        asert.assertAll();
    }

    private String getPastDay() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.YYYY");
        Instant yesterday = Instant.now().minus(5, ChronoUnit.DAYS);
        return dateFormat.format(Date.from(yesterday));
    }
}