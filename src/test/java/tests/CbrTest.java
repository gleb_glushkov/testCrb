package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import steps.Steps;

import static utils.Constants.BASE_URL;

public class CbrTest {

    private WebDriver driver;
    private Steps steps;

    @BeforeClass
    public void beforeClass() {
        driver = new ChromeDriver();
        steps = new Steps(driver);

        steps.goTo(BASE_URL);
    }

    @AfterClass
    public void tearDown() {
        driver.close();
    }

    @Test
    public void testTodayCourse() {
        steps.saveCurrentDayRate();
        steps.savePastDayRate();
        steps.compareRate();
    }
}