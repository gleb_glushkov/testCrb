package utils;

import org.testng.Assert;

import javax.xml.stream.*;
import javax.xml.stream.events.XMLEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class XmlHelper {

    private static final XMLInputFactory INPUT_FACTORY = XMLInputFactory.newInstance();
    private static final XMLOutputFactory OUTPUT_FACTORY = XMLOutputFactory.newInstance();

    public static void writeMapXmlFile(Map<String, Double> map, String toFile) {
        try {
            XMLStreamWriter writer = OUTPUT_FACTORY.createXMLStreamWriter(
                    new FileWriter("./src/main/resources/" + toFile, false));
            writer.writeStartDocument();
            writer.writeStartElement("exchangeRate");
            map.forEach((letterCode, rate) -> {
                try {
                    writer.writeStartElement("add");
                    writer.writeAttribute("key", letterCode);
                    writer.writeAttribute("value", rate.toString());
                    writer.writeEndElement();
                } catch (XMLStreamException e) {
                    e.printStackTrace();
                }
            });
            writer.writeEndElement();
            writer.writeEndDocument();
            writer.flush();
            writer.close();
        } catch (XMLStreamException | IOException e) {
            Assert.fail("Error write xml file", e);
        }
    }

    public static Map<String, Double> readMapXmlFile(String fromFile) {
        Map<String, Double> map = new HashMap<>();
        try (StaxStreamProcessor processor = new StaxStreamProcessor(Files.newInputStream(Paths.get("./src/main/resources/" + fromFile)))) {
            XMLStreamReader reader = processor.getReader();
            while (reader.hasNext()) {
                int event = reader.next();
                if (event == XMLEvent.START_ELEMENT &&
                        "add".equals(reader.getLocalName())) {
                        map.put(reader.getAttributeValue(null, "key"),
                                Double.parseDouble(reader.getAttributeValue(null, "value")));
                }
            }
        } catch (XMLStreamException | IOException e) {
            Assert.fail("Error read xml file", e);
        }
        return map;
    }

    public static class StaxStreamProcessor implements AutoCloseable {
        private final XMLStreamReader reader;

        StaxStreamProcessor(InputStream is) throws XMLStreamException {
            reader = INPUT_FACTORY.createXMLStreamReader(is);
        }

        XMLStreamReader getReader() {
            return reader;
        }

        @Override
        public void close() {
            if (reader != null) {
                try {
                    reader.close();
                } catch (XMLStreamException e) {
                    // empty
                }
            }
        }
    }
}